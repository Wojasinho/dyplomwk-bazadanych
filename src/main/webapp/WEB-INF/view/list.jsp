<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%--<link rel="stylesheet"--%>
          <%--href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">--%>
    <link rel="stylesheet" href="/css/style.css">

    <title>Books</title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>Books</h1>
            <p>This page contains all informations about books</p>
        </div>
    </div>
</section>

<section class="container">

    <div class="row">
        <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
            <div class="thumbnail">
                <div class="caption">
                    <h3>aaa</h3>
                    <p>aaa</p>
                    <p>aa</p>
                    <p>aaaaa</p>
                    <p>
                        <a
                                href=" <spring:url value="/books/book?id=1" /> "
                                class="btn btn-primary"> <span
                                class="glyphicon-info-sign glyphicon" /></span> Details
                        </a>
                    </p>

                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
            <div class="thumbnail">
                <div class="caption">
                    <h3>aaa</h3>
                    <p>aaa</p>
                    <p>aa</p>
                    <p>aaaaa</p>
                    <p>
                        <a
                                href=" <spring:url value="/books/book?id=1" /> "
                                class="btn btn-primary"> <span
                                class="glyphicon-info-sign glyphicon" /></span> Details
                        </a>
                    </p>

                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
            <div class="thumbnail">
                <div class="caption">
                    <h3>aaa</h3>
                    <p>aaa</p>
                    <p>aa</p>
                    <p>aaaaa</p>
                    <p>
                        <a
                                href=" <spring:url value="/books/book?id=1" /> "
                                class="btn btn-primary"> <span
                                class="glyphicon-info-sign glyphicon" /></span> Details
                        </a>
                    </p>

                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
            <div class="thumbnail">
                <div class="caption">
                    <h5><B>WYSOKOWYDAJNE ALGORYTMY SKUPIEN W CHMURZE OBLICZENIOWEJ</B></h5>
                    <p>aaa</p>
                    <p>aa</p>
                    <p>aaaaa</p>
                    <p>
                        <a
                                href=" <spring:url value="/books/book?id=1" /> "
                                class="btn btn-primary"> <span
                                class="glyphicon-info-sign glyphicon" /></span> Details
                        </a>
                    </p>

                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
            <div class="thumbnail">
                <div class="caption">
                    <h3>aaa</h3>
                    <p>aaa</p>
                    <p>aa</p>
                    <p>aaaaa</p>
                    <p>
                        <a
                                href=" <spring:url value="/books/book?id=1" /> "
                                class="btn btn-primary"> <span
                                class="glyphicon-info-sign glyphicon" /></span> Details
                        </a>
                    </p>

                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
            <div class="thumbnail">
                <div class="caption">
                    <h3>aaa</h3>
                    <p>aaa</p>
                    <p>aa</p>
                    <p>aaaaa</p>
                    <p>
                        <a
                                href=" <spring:url value="/books/book?id=1" /> "
                                class="btn btn-primary"> <span
                                class="glyphicon-info-sign glyphicon" /></span> Details
                        </a>
                    </p>

                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
            <div class="thumbnail">
                <div class="caption">
                    <h3>aaa</h3>
                    <p>aaa</p>
                    <p>aa</p>
                    <p>aaaaa</p>
                    <p>
                        <a
                                href=" <spring:url value="/books/book?id=1" /> "
                                class="btn btn-primary"> <span
                                class="glyphicon-info-sign glyphicon" /></span> Details
                        </a>
                    </p>

                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
            <div class="thumbnail">
                <div class="caption">
                    <h3></h3>
                    <p>aaa</p>
                    <p>aa</p>
                    <p>aaaaa</p>
                    <p>
                        <a
                                href=" <spring:url value="/books/book?id=1" /> "
                                class="btn btn-primary"> <span
                                class="glyphicon-info-sign glyphicon" /></span> Details
                        </a>
                    </p>

                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
            <div class="thumbnail">
                <div class="caption">
                    <h3>aaa</h3>
                    <p>aaa</p>
                    <p>aa</p>
                    <p>aaaaa</p>
                    <p>
                        <a
                                href=" <spring:url value="/books/book?id=1" /> "
                                class="btn btn-primary"> <span
                                class="glyphicon-info-sign glyphicon" /></span> Details
                        </a>
                    </p>

                </div>
            </div>
        </div>

</section>

</body>
</html>
