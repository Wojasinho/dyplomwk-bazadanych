<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet"
          href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

    <link rel="stylesheet" href="/css/style.css">

    <title>Books</title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h2>${greeting}</h2>
            <p>${info}</p>
        </div>
    </div>
</section>

<section class="container">

    <h1>Prace Dyplomowe</h1>


    <table class="responstable">

        <tr>
            <th></th>
            <th>ID</th>
            <th>Temat pracy [PL]</th>
            <th>Temat pracy [ENG]</th>
            <th>Typ pracy</th>
        </tr>
        <c:forEach items="${diplomaThesisList}" var="diplomaThesis">

        <tr>
            <td><input type="radio"/></td>
            <td>${diplomaThesis.id}</td>
            <td>${diplomaThesis.tematPl}</td>
            <td>${diplomaThesis.tematEng}</td>
            <td>${diplomaThesis.typPracy}</td>
        </tr>

        </c:forEach>

    <%--<tr>--%>
            <%--<td><input type="radio"/></td>--%>
            <%--<td>Steffie</td>--%>
            <%--<td>Foo</td>--%>
            <%--<td>01/01/1978</td>--%>
            <%--<td>Spouse</td>--%>
        <%--</tr>--%>

        <%--<tr>--%>
            <%--<td><input type="radio"/></td>--%>
            <%--<td>Stan</td>--%>
            <%--<td>Foo</td>--%>
            <%--<td>01/01/1994</td>--%>
            <%--<td>Son</td>--%>
        <%--</tr>--%>

        <%--<tr>--%>
            <%--<td><input type="radio"/></td>--%>
            <%--<td>Stella</td>--%>
            <%--<td>Foo</td>--%>
            <%--<td>01/01/1992</td>--%>
            <%--<td>Daughter</td>--%>
        <%--</tr>--%>

    </table>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js'></script>

</section>

</body>
</html>
