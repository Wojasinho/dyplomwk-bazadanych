<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet"
          href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">


    <link rel="stylesheet" href="css/stylesearch.css">
    <title>System Dyplom</title>




    <script type="text/javascript">
        <!--
        function toggle_visibility(id) {
            var e = document.getElementById(id);
            if(e.style.display == 'block')
                e.style.display = 'none';
            else
                e.style.display = 'block';
        }
        //-->
    </script>

    <style type="text/css">

        #popupBoxOnePosition{
            top: 0; left: 0; position: fixed; width: 100%; height: 120%;
            background-color: rgba(0,0,0,0.7); display: none;
        }
        #popupBoxTwoPosition{
            top: 0; left: 0; position: fixed; width: 100%; height: 120%;
            background-color: rgba(0,0,0,0.7); display: none;
        }#popupBoxThreePosition{
             top: 0; left: 0; position: fixed; width: 100%; height: 120%;
             background-color: rgba(0,0,0,0.7); display: none;
         }
        .popupBoxWrapper{
            width: 550px; margin: 50px auto; text-align: left;
        }
        .popupBoxContent{
            background-color: #FFF; padding: 15px;
        }

    </style>



</head>
<body>

<section>
    <div class="jumbotron">
        <div class="container">
            <h5>Sortuj / Wyszukaj pracę dyplomową</h5>
            <p>Poniżej znajduje się formularz wyszukania / sortowania tematu pracy dyplomowej</p>
        </div>
        <%--<a href="<c:url value="/j_spring_security_logout" />"--%>
        <%--class="btn btn-danger btn-mini pull-right">logout</a>--%>
    </div>

<form class="search_bar large">
    <div class="search_dropdown" style="width: 16px;">
        <span>Szukaj prace dyplomową</span>
        <ul>
            <li class="selected">Slowo kluczowe</li>
            <li>Temat pracy [PL]</li>
            <li>Temat pracy [ENG]</li>
            <li>Kierunek</li>
            <li>Typ pracy</li>
            <li>Imie i Nazwisko promotora</li>


        </ul>
    </div>

    <input type="text" placeholder="Szukaj pracę " />

    <button type="submit" value="Search">Search</button>



</form>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>


<script  src="js/index.js"></script>

<section class="container">

    <h1>Prace Dyplomowe</h1>


    <table class="responstable">

        <tr>
            <th></th>
            <th>ID</th>
            <th>Temat pracy [PL]</th>
            <th>Temat pracy [ENG]</th>
            <th>Typ pracy</th>
            <th>Imie i Nazwisko promotora</th>
            <th>Kierunek</th>


        </tr>
        <c:forEach items="${diplomaThesisList}" var="diplomaThesis">

            <tr>
                <td><input type="radio"/></td>
                <td>${diplomaThesis.id}</td>
                <td>${diplomaThesis.tematPl}</td>
                <td>${diplomaThesis.tematEng}</td>
                <td>${diplomaThesis.typPracy}</td>
            </tr>

        </c:forEach>


    </table>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js'></script>

</section>
</section>
<section>    <div id="popupBoxOnePosition">
    <div class="popupBoxWrapper">
        <div class="popupBoxContent">
            <h3>Niestety ale nie znaleziono szukanej przez Ciebie pracy dyplomowej "ANIA Z ZIELONEGO WZGORZA"</h3>
            <p>Sproboj ponownie wpisać poprawny temat pracy, badz wybierz inne kryteria</p>
            <p>Zamknij okienko <a href="javascript:void(0)" onclick="toggle_visibility('popupBoxOnePosition');">tutaj</a> aby wyszukać temat pracy jeszcze raz </p>
        </div>
    </div>
</div>

    <div id="wrapper">

        <p> <a href="javascript:void(0)" onclick="toggle_visibility('popupBoxOnePosition');">.</a></p>

    </div><!-- wrapper end --></section>
</body>





</html>
