<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet"
          href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <title>System Dyplom</title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h2>${greeting}</h2>
            <p>${info}</p>
        </div>
    </div>
</section>


<section class="container">

    <div class="row">
        <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Wyświetl tematy</h3>
                    <%--<p></p>--%>
                    <p>
                        <a href="/list" class="btn btn-default"> <span
                                class="glyphicon-info-sign glyphicon" /></span> Wyświetl wszystkie tematy
                        </a>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Sortuj listę tematów prac dyplomowych</h3>
                    <%--<p>Create new book</p>--%>
                    <p>
                        <a href="/sortThesis" class="btn btn-default"> <span
                                class="glyphicon-info-sign glyphicon" /></span> Dodaj nowy temat
                        </a>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Dodaj temat pracy dyplomowej</h3>
                    <%--<p>Create new book</p>--%>
                    <p>
                        <a href="/formularAddThesis" class="btn btn-default"> <span
                                class="glyphicon-info-sign glyphicon" /></span> Dodaj nowy temat
                        </a>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Usun temat pracy dyplomowej</h3>
                    <%--<p>Create new book</p>--%>
                    <p>
                        <a href="/formularRemove" class="btn btn-default"> <span
                                class="glyphicon-info-sign glyphicon" /></span> Usun temat
                        </a>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Modyfikuj temat pracy dyplomowej</h3>
                    <%--<p>Create new book</p>--%>
                    <p>
                        <a href="/updateThesis" class="btn btn-default"> <span
                                class="glyphicon-info-sign glyphicon" /></span> Modyfikuj pracę dyplomową
                        </a>
                    </p>
                </div>
            </div>
        </div>



        <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Generuj raport Prac dyplomowych</h3>
                    <%--<p>Create new book</p>--%>
                    <p>
                        <a href="/generateRaport" class="btn btn-default"> <span
                                class="glyphicon-info-sign glyphicon" /></span> Generuj raport
                        </a>
                    </p>
                </div>
            </div>
        </div>





    </div>

</section>
</body>
</html>
