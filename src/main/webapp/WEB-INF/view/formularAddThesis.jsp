<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet"
          href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <title>SYSTEM DYPLOM</title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>Dodaj nową pracę dyplomową</h1>
            <p>System Dyplom</p>
        </div>
        <%--<a href="<c:url value="/j_spring_security_logout" />"--%>
           <%--class="btn btn-danger btn-mini pull-right">logout</a>--%>
    </div>
</section>
<section class="container">
    <form:form modelAttribute="diplomaThesis" class="form-horizontal">
        <fieldset>
            <legend>Wprowadź poniżej w formularzu Pracę Dyplomową</legend>

            <!-- Sample template for some fields in Book Entity -->
            <div class="form-group">
                <label class="control-label col-lg-2" for="tematPl">Tytuł Pracy w języku polskim</label>
                <div class="col-lg-10">
                    <form:input id="tematPl" path="tematPl" type="text"
                                class="form:input-large" />
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-lg-2" for="tematEng">Tytuł Pracy w języku angielskim</label>
                <div class="col-lg-10">
                    <form:input id="tematEng" path="tematEng" type="text"
                                class="form:input-large" />
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-2" for="typPracy">Typ Pracy [magisterska/inżynierska]</label>
                <div class="col-lg-10">
                    <form:input id="typPracy" path="typPracy" type="text"
                                class="form:input-large" />
                </div>
            </div>


            <%--<div class="form-group">--%>
                <%--<label class="control-label col-lg-2" for="typPracy">status</label>--%>
                <%--<div class="col-lg-10">--%>
                    <%--<form:radiobutton path="status" value="FREE" />--%>
                    <%--Free--%>
                    <%--<form:radiobutton path="status" value="LOAN" />--%>
                    <%--Loan--%>
                    <%--<form:radiobutton path="status" value="MISSING" />--%>
                    <%--Missing--%>
                <%--</div>--%>
            </div>

    </form:form>


    <form:form modelAttribute="student" class="form-horizontal">

            <!-- Sample template for some fields in Book Entity -->
            <div class="form-group">
                <label class="control-label col-lg-2" for="imie">Imię studenta</label>
                <div class="col-lg-10">
                    <form:input id="imie" path="imie" type="text"
                                class="form:input-large" />
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-lg-2" for="nazwisko">Nazwisko studenta</label>
                <div class="col-lg-10">
                    <form:input id="nazwisko" path="nazwisko" type="text"
                                class="form:input-large" />
                </div>
            </div>

            <div class="form-group">
            <label class="control-label col-lg-2" for="nrIndeksu">Numer indeksu</label>
            <div class="col-lg-10">
                <form:input id="nrIndeksu" path="nrIndeksu" type="number"
                            class="form:input-large" />
            </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-2" for="email">Adres E-Mail</label>
                <div class="col-lg-10">
                    <form:input id="email" path="email" type="text"
                                class="form:input-large" />
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-2" for="">Typ pracy</label>
                <div class="col-lg-10">
                    <form:input id="" path="" type="number"
                                class="form:input-large" />
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-2" for="">Imię i nazwisko promotora</label>
                <div class="col-lg-10">
                    <form:input id="" path="" type="text"
                                class="form:input-large" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2" for="">Ocena Pracy</label>
                <div class="col-lg-10">
                    <form:input id="" path="" type="number"
                                class="form:input-large" />
                </div>
            </div>
            <%--<div class="form-group">--%>
                <%--<label class="control-label col-lg-2" for="kierunkiStudenta">Kierunek</label>--%>
                <%--<div class="col-lg-10">--%>
                    <%--<form:input id="kierunkiStudenta" path="kierunkiStudenta" type="text"--%>
                                <%--class="form:input-large" />--%>
                <%--</div>--%>
            <%--</div>--%>


            </div>
        </fieldset>

    </form:form>

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <input type="submit" id="btnAdd" class="btn btn-primary"
                       value="Dodaj nową pracę" />
            </div>
        </div>

<p>
    <a href="<spring:url value="/" />" class="btn btn-default">
        <span class="glyphicon-hand-left glyphicon"></span> back </a>
</p>

</section>

</body>
</html>
