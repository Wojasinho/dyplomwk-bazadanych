<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet"
          href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <title>SYSTEM DYPLOM</title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>Usuń pracę dyplomową</h1>
            <p>System Dyplom</p>
        </div>
        <%--<a href="<c:url value="/j_spring_security_logout" />"--%>
        <%--class="btn btn-danger btn-mini pull-right">logout</a>--%>
    </div>
</section>
<section class="container">
    <form:form modelAttribute="diplomaThesis" class="form-horizontal">
    <fieldset>
        <legend>Wprowadź poniżej w formularzu tytuł pracy dyplomowej, który chcesz usunąć</legend>

        <!-- Sample template for some fields in Book Entity -->
        <div class="form-group">
            <label class="control-label col-lg-2" for="tematPl">Tytuł Pracy w języku polskim</label>
            <div class="col-lg-10">
                <form:input id="tematPl" path="tematPl" type="text"
                            class="form:input-large" />
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-2" for="tematPl">Imie i nazwisko Autora</label>
            <div class="col-lg-10">
                <form:input id="" path="" type="text"
                            class="form:input-large" />
            </div>
        </div>


        </div>
    </fieldset>
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <input type="submit" id="btnAdd" class="btn btn-primary"
                       value="Create" />
            </div>
        </div>
        </form:form>




    <p>
        <a href="<spring:url value="/" />" class="btn btn-default">
            <span class="glyphicon-hand-left glyphicon"></span> back </a>
    </p>

</section>

</body>
</html>
