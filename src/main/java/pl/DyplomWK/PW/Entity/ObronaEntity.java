package pl.DyplomWK.PW.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="OBRONA")
public class ObronaEntity implements Serializable{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="DATA_OBRONY", nullable = false)
    private Date data;

    @ManyToOne(fetch = FetchType.LAZY,cascade={CascadeType.ALL})
    @JoinColumn(name = "ID_PRACA_DYPLOMOWA_FK", nullable = false)
    private PracaDyplomowaEntity pracaDyplomowaEntity;

    @ManyToOne(fetch = FetchType.LAZY,cascade={CascadeType.ALL})
    @JoinColumn(name = "ID_OCENA_KONCOWA_FK", nullable = false)
    private OcenaEntity ocenaKoncowa;
//
//    @ManyToMany(mappedBy = "obronyStudenta")
//    private List<StudentEntity> studenciObrony = new ArrayList<>();

    @ManyToMany(mappedBy = "obronyStudenta",cascade={CascadeType.ALL})
    private List<StudentEntity> studenciObrony = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
//
//    public List<StudentEntity> getStudenciObrony() {
//        return studenciObrony;
//    }
//
//    public void setStudenciObrony(List<StudentEntity> studenciObrony) {
//        this.studenciObrony = studenciObrony;
//    }

    public OcenaEntity getOcenaKoncowa() {
        return ocenaKoncowa;
    }

    public void setOcenaKoncowa(OcenaEntity ocenaKoncowa) {
        this.ocenaKoncowa = ocenaKoncowa;
    }
}
