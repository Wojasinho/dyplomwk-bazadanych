package pl.DyplomWK.PW.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PRACA_DYPLOMOWA")
public class PracaDyplomowaEntity implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "TEMAT_PL", nullable = false, length = 100)
    private String tematPl;

    @Column(name = "TEMAT_ENG", nullable = true, length = 100)
    private String tematEng;

    @Column(name = "TYP_PRACY", nullable = false, length = 15)
    private String typPracy;

    @ManyToMany(fetch=FetchType.LAZY,cascade={CascadeType.ALL})
    @JoinTable(name = "OPIS_PRACY_DYPLOMOWEJ",
            joinColumns = @JoinColumn(name = "ID_PRACA_DYPLOMOWA",nullable = false),
            inverseJoinColumns = @JoinColumn(name = "ID_SLOWA",nullable = false))
    private List<SlowaKluczoweEntity> opisPracyDyplomowej = new ArrayList<>();



    public PracaDyplomowaEntity() {
        this.tematPl = tematPl;
        this.tematEng = tematEng;
        this.typPracy = typPracy;
        this.opisPracyDyplomowej = opisPracyDyplomowej;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTematPl() {
        return tematPl;
    }

    public void setTematPl(String tematPl) {
        this.tematPl = tematPl;
    }

    public String getTematEng() {
        return tematEng;
    }

    public void setTematEng(String tematEng) {
        this.tematEng = tematEng;
    }

    public String getTypPracy() {
        return typPracy;
    }

    public void setTypPracy(String typPracy) {
        this.typPracy = typPracy;
    }

    public List<SlowaKluczoweEntity> getOpisPracyDyplomowej() {
        return opisPracyDyplomowej;
    }

    public void setOpisPracyDyplomowej(List<SlowaKluczoweEntity> opisPracyDyplomowej) {
        this.opisPracyDyplomowej = opisPracyDyplomowej;
    }
}
