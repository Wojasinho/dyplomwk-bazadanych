package pl.DyplomWK.PW.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="STUDENT")
public class StudentEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="IMIE",nullable = false, length = 20)
    private String imie;

    @Column(name="NAZWISKO",nullable = false, length = 20)
    private String nazwisko;

    @Column(name = "NR_INDEKSU", nullable = false, length = 6)
    private int nrIndeksu;

    @Column(name="EMAIL", nullable = false, length = 40)
    private String email;

    @ManyToMany(fetch=FetchType.LAZY,cascade={CascadeType.ALL})
    @JoinTable(name = "KIERUNKI_STUDENTA",
            joinColumns = @JoinColumn(name = "ID_STUDENT",nullable = true),
            inverseJoinColumns = @JoinColumn(name = "ID_KIERUNEK",nullable = true)
    )
    private List<KierunekEntity> kierunkiStudenta = new ArrayList<>();


    @ManyToMany(fetch=FetchType.LAZY,cascade={CascadeType.ALL})
    @JoinTable(name = "OBRONY_STUDENTA",
            joinColumns = @JoinColumn(name = "ID_STUDENT",nullable = true),
            inverseJoinColumns = @JoinColumn(name = "ID_OBRONA",nullable = true))
    private List<ObronaEntity> obronyStudenta = new ArrayList<>();

    public StudentEntity() {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nrIndeksu = nrIndeksu;
        this.email = email;
        this.kierunkiStudenta = kierunkiStudenta;
        this.obronyStudenta = obronyStudenta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getNrIndeksu() {
        return nrIndeksu;
    }

    public void setNrIndeksu(int nrIndeksu) {
        this.nrIndeksu = nrIndeksu;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<KierunekEntity> getKierunkiStudenta() {
        return kierunkiStudenta;
    }

    public void setKierunkiStudenta(List<KierunekEntity> kierunkiStudenta) {
        this.kierunkiStudenta = kierunkiStudenta;
    }
//
//    public List<ObronaEntity> getObronyStudenta() {
//        return obronyStudenta;
//    }
//
//    public void setObronyStudenta(List<ObronaEntity> obronyStudenta) {
//        this.obronyStudenta = obronyStudenta;
//    }
}
