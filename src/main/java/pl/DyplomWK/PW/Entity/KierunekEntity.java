package pl.DyplomWK.PW.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table (name="KIERUNEK")
public class KierunekEntity implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="KIERUNEK", nullable = false, length = 30)
    private String kierunek;

    @ManyToMany(mappedBy = "kierunkiStudenta",cascade={CascadeType.ALL})
    private List<StudentEntity> studenciDanegoKierunku = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKierunek() {
        return kierunek;
    }

    public void setKierunek(String kierunek) {
        this.kierunek = kierunek;
    }

    public List<StudentEntity> getStudenciDanegoKierunku() {
        return studenciDanegoKierunku;
    }

    public void setStudenciDanegoKierunku(List<StudentEntity> studenciDanegoKierunku) {
        this.studenciDanegoKierunku = studenciDanegoKierunku;
    }
}
