package pl.DyplomWK.PW.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table (name = "PRACOWNIK_UCZELNI")
public class PracownikUczelniEntity implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "IMIE", nullable = false, length = 20)
    private String imie;

    @Column(name = "NAZWISKO", nullable = false, length = 20)
    private String nazwisko;

    @Column(name = "EMAIL", nullable = false, length = 40)
    private String email;

    @Column(name = "STOPIEN", nullable = false, length = 20)
    private String stopien;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStopien() {
        return stopien;
    }

    public void setStopien(String stopien) {
        this.stopien = stopien;
    }
}
