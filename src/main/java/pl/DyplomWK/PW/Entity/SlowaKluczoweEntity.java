package pl.DyplomWK.PW.Entity;

import javax.persistence.Entity;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name="SLOWA_KLUCZOWE")
public class SlowaKluczoweEntity implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="SLOWO_KLUCZOWE",nullable = false, length = 20)
    private String slowoKluczowe;

    @ManyToMany(mappedBy = "opisPracyDyplomowej",cascade={CascadeType.ALL})
    private List<PracaDyplomowaEntity> praceDyplomoweDanegoSlowa = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSlowoKluczowe() {
        return slowoKluczowe;
    }

    public void setSlowoKluczowe(String slowoKluczowe) {
        this.slowoKluczowe = slowoKluczowe;
    }

    public List<PracaDyplomowaEntity> getPraceDyplomoweDanegoSlowa() {
        return praceDyplomoweDanegoSlowa;
    }

    public void setPraceDyplomoweDanegoSlowa(List<PracaDyplomowaEntity> praceDyplomoweDanegoSlowa) {
        this.praceDyplomoweDanegoSlowa = praceDyplomoweDanegoSlowa;
    }
}
