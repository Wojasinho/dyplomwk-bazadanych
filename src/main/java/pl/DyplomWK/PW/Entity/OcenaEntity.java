package pl.DyplomWK.PW.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table (name="OCENA")
public class OcenaEntity implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "OCENA", nullable = false, length = 20)
    private String ocena;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOcena() {
        return ocena;
    }

    public void setOcena(String ocena) {
        this.ocena = ocena;
    }
}
