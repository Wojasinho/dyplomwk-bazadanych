package pl.DyplomWK.PW.Entity;

import javax.persistence.*;

@Entity
@Table(name="OPIEKUNOWIE_PRAC_DYPLOMOWYCH")
public class OpiekunowiePracDyplomowychEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY,cascade={CascadeType.ALL})
    @JoinColumn(name = "ID_PRACA_DYPLOMOWA", nullable = false)
    private PracaDyplomowaEntity pracaDyplomowaEntity;

    @ManyToOne(fetch = FetchType.LAZY,cascade={CascadeType.ALL})
    @JoinColumn(name = "ID_PRACOWNIK_UCZELNI", nullable = false)
    private PracownikUczelniEntity pracownikUczelniEntity;

    @Column(name="CZY_PROMOTOR",nullable = false)
    private boolean czyPromotor;

//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JoinColumn(name = "id_placowki_fk")
//    private FacilityEntity facility;
//
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="ID_OCENA_FK",nullable = false)
    private OcenaEntity idOcenaFk;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PracaDyplomowaEntity getPracaDyplomowaEntity() {
        return pracaDyplomowaEntity;
    }

    public void setPracaDyplomowaEntity(PracaDyplomowaEntity pracaDyplomowaEntity) {
        this.pracaDyplomowaEntity = pracaDyplomowaEntity;
    }

    public PracownikUczelniEntity getPracownikUczelniEntity() {
        return pracownikUczelniEntity;
    }

    public void setPracownikUczelniEntity(PracownikUczelniEntity pracownikUczelniEntity) {
        this.pracownikUczelniEntity = pracownikUczelniEntity;
    }

    public boolean isCzyPromotor() {
        return czyPromotor;
    }

    public void setCzyPromotor(boolean czyPromotor) {
        this.czyPromotor = czyPromotor;
    }

    public OcenaEntity getIdOcenaFk() {
        return idOcenaFk;
    }

    public void setIdOcenaFk(OcenaEntity idOcenaFk) {
        this.idOcenaFk = idOcenaFk;
    }
}

