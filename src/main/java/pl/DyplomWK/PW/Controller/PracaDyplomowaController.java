package pl.DyplomWK.PW.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.DyplomWK.PW.Dao.PracaDyplomowaDao;
import pl.DyplomWK.PW.Entity.PracaDyplomowaEntity;
import pl.DyplomWK.PW.Entity.StudentEntity;
import pl.DyplomWK.PW.constants.ModelConstants;
import pl.DyplomWK.PW.constants.ViewNames;

import java.util.List;

@Controller
@RequestMapping("/list")

public class PracaDyplomowaController {

    private static final String INFO_TEXT = "Poniżej znajdują się dostępne tematy Prac Dyplomowych";
    private static final String WELCOME = "Lista dostępnych prac dyplomowych";

    @Autowired
    private PracaDyplomowaDao pracaDyplomowaDao;

    @RequestMapping(method = RequestMethod.GET)
    public String lists(Model model) {
        List<PracaDyplomowaEntity> listOfDiplomaThesis = pracaDyplomowaDao.findAll();
        model.addAttribute("diplomaThesisList", listOfDiplomaThesis);
        model.addAttribute(ModelConstants.GREETING, WELCOME);
        model.addAttribute(ModelConstants.INFO, INFO_TEXT);
        return ViewNames.LIST;
    }


    /**
     * Binder initialization
     */
    @InitBinder
    public void initialiseBinder(WebDataBinder binder) {
        binder.setAllowedFields("id", "tematPl", "tematEng", "typPracy");
    }


}
