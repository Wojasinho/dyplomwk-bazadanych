package pl.DyplomWK.PW.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.DyplomWK.PW.Dao.PracaDyplomowaDao;
import pl.DyplomWK.PW.Dao.StudentDao;
import pl.DyplomWK.PW.Entity.PracaDyplomowaEntity;
import pl.DyplomWK.PW.Entity.StudentEntity;
import pl.DyplomWK.PW.constants.ModelConstants;
import pl.DyplomWK.PW.constants.ViewNames;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Controller
public class HomeController {
    @Autowired
    private PracaDyplomowaDao pracaDyplomowaDao;

    @Autowired
    private StudentDao studentDao;

    @PersistenceContext
    protected EntityManager entityManager;


    private static final String INFO_TEXT = "Poniżej znajdziesz możliwość zarządzania Systemem";
    private static final String WELCOME = "Witaj w Systemie Dyplom Politechnika Warszawska";

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        model.addAttribute(ModelConstants.GREETING, WELCOME);
        model.addAttribute(ModelConstants.INFO, INFO_TEXT);
        return ViewNames.WELCOME;
    }

    @RequestMapping(value = "/formularSort", method = RequestMethod.GET)
    public String showFormularSort(Model model) {
        model.addAttribute(ModelConstants.GREETING, WELCOME);
        model.addAttribute(ModelConstants.INFO, INFO_TEXT);
        return ViewNames.FORMULARSORT;
    }

    @RequestMapping(value = "/formularAddThesis", method = RequestMethod.GET)
    public String showFormularAddThesis(Model model) {
        model.addAttribute("diplomaThesis", new PracaDyplomowaEntity());
        model.addAttribute("student", new StudentEntity());

//        model.addAttribute(ModelConstants.GREETING, WELCOME);
//        model.addAttribute(ModelConstants.INFO, INFO_TEXT);
        return ViewNames.FORMULARADD;
    }

    @RequestMapping(value = "/formularRemove", method = RequestMethod.GET)
    public String showFormularRemove(Model model) {
        model.addAttribute("diplomaThesis", new PracaDyplomowaEntity());

//        model.addAttribute(ModelConstants.GREETING, WELCOME);
//        model.addAttribute(ModelConstants.INFO, INFO_TEXT);
        return ViewNames.FORMULARREMOVE;
    }

    @RequestMapping(value = "/formularAddThesis", method = RequestMethod.POST)
    public String submitAdd(@ModelAttribute("diplomaThesis") PracaDyplomowaEntity pracaDyplomowaEntity,
                         @ModelAttribute("student")StudentEntity studentEntity,

                         BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "error";
        }
//        else if (!pracaDyplomowaEntity.getTematPl().isEmpty() && !pracaDyplomowaEntity.getTematEng().isEmpty() &&
//                !pracaDyplomowaEntity.getTypPracy().isEmpty() ) {


        pracaDyplomowaDao.save(pracaDyplomowaEntity);
        studentDao.save(studentEntity);
        return "redirect:/";

    }
    @Transactional
    @RequestMapping(value = "/formularRemove", method = RequestMethod.POST)
    public String submitRemove(@ModelAttribute("diplomaThesis") PracaDyplomowaEntity pracaDyplomowaEntity,
                         BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "error";
        }
//        else if (!pracaDyplomowaEntity.getTematPl().isEmpty() && !pracaDyplomowaEntity.getTematEng().isEmpty() &&
//                !pracaDyplomowaEntity.getTypPracy().isEmpty() ) {

        PracaDyplomowaEntity n = pracaDyplomowaDao.findByTematPl(pracaDyplomowaEntity.getTematPl());
        pracaDyplomowaDao.removeByTematPl(n.getTematPl());
        return "redirect:/";

    }

    @RequestMapping(value =  "/login", method = RequestMethod.GET)
    public String login(Model model) {
//        model.addAttribute(ModelConstants.GREETING, WELCOME);
//        model.addAttribute(ModelConstants.INFO, INFO_TEXT);
        return ViewNames.LOGIN;
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String popUp(Model model) {
//        model.addAttribute(ModelConstants.GREETING, WELCOME);
//        model.addAttribute(ModelConstants.INFO, INFO_TEXT);
        return "test";
    }
}









