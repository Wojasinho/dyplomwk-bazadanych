package pl.DyplomWK.PW.constants;

import java.io.Serializable;

public final class ModelConstants implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String GREETING = "greeting";
    public static final String INFO = "info";
}