package pl.DyplomWK.PW.constants;

import java.io.Serializable;

public final class ViewNames implements Serializable {

    public static final String WELCOME = "welcome";
    public static final String LIST = "list_backup";
    public static final String FORMULARSORT="formularSort";
    public static final String FORMULARADD="formularAddThesis";
    public static final String FORMULARREMOVE="formularRemove";
    public static final String LOGIN="login";

}
