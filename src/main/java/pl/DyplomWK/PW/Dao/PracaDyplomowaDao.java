package pl.DyplomWK.PW.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.DyplomWK.PW.Entity.PracaDyplomowaEntity;

import java.util.List;
@Repository
@Transactional
public interface PracaDyplomowaDao extends CrudRepository<PracaDyplomowaEntity, Long> {

    List<PracaDyplomowaEntity> findAll();
    PracaDyplomowaEntity save(PracaDyplomowaEntity pracaDyplomowaEntity);
    PracaDyplomowaEntity findByTematPl(String tematPl);
    PracaDyplomowaEntity removeByTematPl(String tematPl);
//    PracaDyplomowaEntity removeById(long id);


}
