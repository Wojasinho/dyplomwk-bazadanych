package pl.DyplomWK.PW.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.DyplomWK.PW.Entity.PracaDyplomowaEntity;
import pl.DyplomWK.PW.Entity.StudentEntity;

@Repository
public interface StudentDao extends CrudRepository<StudentEntity, Long> {

    StudentEntity save(StudentEntity studentEntity);
    
}

